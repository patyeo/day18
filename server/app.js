var express = require("express");
var path = require('path');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));


var node_port = process.env.PORT || 4000;

const CLIENT_FOLDER = path.join(__dirname, '/../client');
const MESSAGES_FOLDER = path.join(CLIENT_FOLDER, '/assets/messages');

app.use(express.static(__dirname + "/../client/"));

require("./routes")(app);

app.use(function (req, res) {
    res.send(404).sendFile(MESSAGES_FOLDER + '/404.html');
});

app.listen(node_port, function () {
    console.log("Server running at http://localhost:" + node_port);
});