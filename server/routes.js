var fs = require ("fs"),
// fs = file system, reading and writing files on the server side
    path = require ("path"),
    multer = require ("multer"),
// multer is to transport / decoding for file transfer from client side to server
    AWS = require ("aws-sdk"),
    multerS3 = require ("multer-S3"),
// this is a connector, that when the system see an file upload, to stream to server
    storage = multer.diskStorage({
// this is to tell multer where to store
        destination: './upload_tmp',
        filename: function(req, file, callback){
            console.log(file);
            console.log(file.originalname);
            callback(null, Date.now() + '-' + file.originalname)
// this is to create a different filename so that it does not get confused when people upload file with same name
        }
    }),
    upload = multer({
        storage: storage
    // this is to assign the place to store
    });

AWS.config.region ="ap-southeast-1";
console.log("AWS init...");
var s3bucket = new AWS.S3({

});

var uploadS3 = new multer({
    storage: multerS3({
        s3:s3bucket,
        bucket: 'patyeo-fsf17-day14',
// this is to store file unto the cloud and not local
        acl: 'public-read',
        metadata: function(req, file, cb){
            console.log(file.fieldname);
            cb(null, {fieldName: file.fieldname});
        },
        key: function(req, file, cb){
            console.log(cb);
            console.log(file.originialname);
            cb(null, Date.now() + '-' + file.originalname);
        }
// this is to tell s3 to reserve a key for the file to be uploaded
    })

})

module.exports = function(app){
    app.post("/upload", upload.single("img-file") , function(req, res){
        console.log("uploading...")
        fs.readFile(req.file.path, function(err, data){
            if(err){
                console.log(err);
            }
            res.status(202).json({size: req.file.size})
            //202 to signify that upload request received but still processing
        })
    });

    app.post("/uploadS3", uploadS3.single("img-file") , function(req, res){
        res.send("Successfully uploaded a file"+ req.file.length)
    });
}
// this is for file upload, so it is always post. create an end point